import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {
  it('create an instance', () => {
    const pipe = new CapitalizePipe();
    expect(pipe).toBeTruthy();
  });
  it('capitalizes a word', () => {
    const pipe = new CapitalizePipe();
    expect(pipe.transform('word')).toBe('Word');
  });
  it('capitalizes a ', () => {
    const pipe = new CapitalizePipe();
    expect(pipe.transform('')).toBe('');
  });
});
