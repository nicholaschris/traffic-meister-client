import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';


import { Vehicle, VehiclesService } from './vehicles.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];
  types: string;
  colors: string[];
  brands: string;
  selectedType: string;
  selectedBrand: string;
  selectedColor: string;
  selectedVehicle: Vehicle;

  constructor(private vehiclesService: VehiclesService) { }

  ngOnInit() {
    this.initData();
  }

  clearFilters() {
    this.initData();
  }

  initData() {
    this.selectedType = '';
    this.selectedBrand = '';
    this.selectedColor = '';
    this.vehiclesService.getVehicles().subscribe(
      data => {
        this.vehicles = data;
        this.types = this.filterTypes(data);
        this.brands = this.filterBrands(data);
        this.colors = this.filterColors(data);
      }
    )
  }

  filterTypes(data) {
    return this.types = data
    .map( item => item.type )
    .filter( (v, i, a) => a.indexOf(v) === i )
  }

  filterBrands(data) {
    return this.brands = data
    .map( item => item.brand )
    .filter( (v, i, a) => a.indexOf(v) === i )
  }

  filterColors(data) {
    return this.colors = data
    .map( item => item.colors )
    .reduce((a, b) => a.concat(b), [] )
    .filter( (v, i, a) => a.indexOf(v) === i )
  }

  onFilter(parameter, value) {
        parameter === 'color' ?
        this.vehicles = this.vehicles.filter( item => item.colors.indexOf(this.selectedColor) > -1 == true ) :
        this.vehicles = this.vehicles.filter( item => item[parameter] == value);
        
        this.types = this.filterTypes(this.vehicles);
        this.brands = this.filterBrands(this.vehicles);
        this.colors = this.filterColors(this.vehicles);
  }

}
