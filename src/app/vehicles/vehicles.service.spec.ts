import { TestBed, inject } from '@angular/core/testing';

import { VehiclesService } from './vehicles.service';

describe('VehiclesService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VehiclesService]
    });
  });

  it('should be created', inject([VehiclesService], (service: VehiclesService) => {
    expect(service).toBeTruthy();
  }));

  it('should be get vehicles', inject([VehiclesService], (service: VehiclesService) => {
    // we just use this normally because we aren't doing an http request
    // if this method changes to use http request please change test
    const vehicles = service.getVehicles();
    expect(vehicles).toBeTruthy();
  }));
});
