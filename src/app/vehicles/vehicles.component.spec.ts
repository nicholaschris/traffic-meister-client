import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }                        from '@angular/platform-browser';
import { DebugElement }              from '@angular/core';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { VehiclesComponent } from './vehicles.component';
import { VehiclesService } from './vehicles.service';

describe('VehiclesComponent', () => {
  let component: VehiclesComponent;
  let fixture: ComponentFixture<VehiclesComponent>;
  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let vehiclesService: VehiclesService;

  const vehicles = [
    {
      id: 1,
      type: 'car',
      brand: 'Bugatti Veyron',
      colors: ['red', 'black', 'blue'],
      img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg/520px-Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg'
    },
    {
      id: 2,
      type: 'airplane',
      brand: 'Boeing 787 Dreamliner',
      colors: ['red', 'white', 'black', 'green'],
      img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg/600px-All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg'
    }
  ]

  const blueVehicles = [
    {
      id: 1,
      type: 'car',
      brand: 'Bugatti Veyron',
      colors: ['red', 'black', 'blue'],
      img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg/520px-Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg'
    }
  ]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, SharedModule, BrowserAnimationsModule],
      declarations: [ VehiclesComponent ],
      providers: [ VehiclesService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesComponent);
    component = fixture.componentInstance;
    vehiclesService = fixture.debugElement.injector.get(VehiclesService);

    // Setup spy on the `getVehicles` method
    spy = spyOn(vehiclesService, 'getVehicles')
        .and.returnValue(Observable.of(vehicles));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show vehicles', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.vehicles).toBe(vehicles);
    });
  }));

  it('should show vehicles by color', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.selectedColor = 'blue';
      component.onFilter('color', 'blue');
      fixture.detectChanges();
      expect(component.vehicles[0].id).toEqual(blueVehicles[0].id);
    });
  }));

  it('should show vehicles by type', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.onFilter('type', 'car');
      fixture.detectChanges();
      expect(component.vehicles[0].id).toEqual(blueVehicles[0].id);
    });
  }));

  it('should clear the filters when asked', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      component.selectedColor = 'blue';
      component.onFilter('color', 'blue');    
      fixture.detectChanges();
      expect(component.vehicles[0].id).toEqual(blueVehicles[0].id);
      component.clearFilters();  
      expect(component.vehicles).toBe(vehicles);
    });
  }));
});
