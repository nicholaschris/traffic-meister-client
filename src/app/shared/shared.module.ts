import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';

// Material UI Components
import { MatButtonModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatChipsModule } from '@angular/material';
import { MatListModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule
  ],
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule
  ]
})
export class SharedModule { }
