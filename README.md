# SytacFrontendAssignmentClient

## The Traffic Meister Application

### Instructions

Install Node [https://nodejs.org/en/download/](https://nodejs.org/en/download/)  
Install Angular-CLI by running `npm install -g @angular/cli`  
Run `npm install` to install dependencies  
This is a public repository you can run `git clone https://bitbucket.org/nicholaschris/traffic-meister-client` to install.

### Testing

Run `ng e2e` to run End-to-end tests  
Run `ng test --single-run --code-coverage` to run unit tests  
You can check for code coverage by `cd coverage` and running `python3 -m http.server 8001` (Requires Python 3)  

Expectations are that all e2e and unit tests pass and that code coverage is ~100%  

### Run locally

Run `ng serve` and follow instructions.

### Build for production

Run `ng build --prod`  
There is also a `Dockerfile` which you can use if you want. See [https://docs.docker.com/](https://docs.docker.com/)

### Notes

Tested on:
 - [x] Chrome for Mac  
 - [x] Firefox for Mac  
 - [x] Safari  
 - [ ] Everywhere else  

This application has not been thoroghly tested across all devices and broswers.

## Angular Development Notes
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
