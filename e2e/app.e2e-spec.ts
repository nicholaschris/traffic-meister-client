import { AppPage } from './app.po';

describe('sytac-frontend-assignment-client App', () => {
  let page: AppPage;
  let TOTAL_VEHICLES = 12;
  let TOTAL_CARS = 4;
  let TOTAL_AIRPLANES = 4;
  let BUGATTI_COLORS = 2;
  let YELLOW_BRANDS = 5;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display the traffic meister logo', done => {
    page.navigateTo();
    page.getLogo().then(logo => expect(logo.isDisplayed()).toBe(true))
    .then(done, done.fail);
  });

  it('should display all possible selections initially', done => {
    page.navigateTo();
    let count = page.getVehicleCount();
    expect(count).toEqual(TOTAL_VEHICLES).then(done, done.fail);
  });

  it('should filter all selections based on type car', done => {
    let value = 'car';
    page.navigateTo();
    page.getTypeSelect(value);
    let count = page.getVehicleCount();
    expect(count).toEqual(TOTAL_CARS).then(done, done.fail);
  });

  it('should filter all selections based on type airplane', done => {
    let value = 'airplane';
    page.navigateTo();
    page.getTypeSelect(value);
    let count = page.getVehicleCount();
    expect(count).toEqual(TOTAL_AIRPLANES).then(done, done.fail);
  });
 
  it('should clear filters when asked', done => {
    let value = 'airplane';
    page.navigateTo();
    page.getTypeSelect(value);
    let count = page.getVehicleCount();
    expect(count).toEqual(4).then(done, done.fail);
    page.getClearFilterButton().click();
    count = page.getVehicleCount();
    expect(count).toEqual(12).then(done, done.fail);
  });

  it('When yellow is selected all types and brands that have no yellow vehicles are filtered out', done => {
    let value = 'yellow';
    page.navigateTo();
    page.getColorSelect(value);
    let count = page.getNumberOfOptions('brand');
    expect(count).toEqual(YELLOW_BRANDS).then(done, done.fail);
  })

  it('When selecting "Bugatti Veyron", only the car type and the available colors are selectable', done => {
    let value = 'Bugatti Veyron';
    page.navigateTo();
    page.getBrandSelect(value);
    let count = page.getNumberOfOptions('color')
    expect(count).toEqual(BUGATTI_COLORS).then(done, done.fail)
  })

});
