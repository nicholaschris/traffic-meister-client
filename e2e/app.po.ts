import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getLogo() {
    return element(by.css('.logo')).getWebElement();
  }

  getVehicleCount() {
    return element.all(by.css('.vehicle')).count();
  }

  getNumberOfOptions(value) {
    element(by.name(value)).click();
    return element.all(by.css(`.mat-option.${value}-option`)).count();
  }
  
  getTypeSelect(value) {
    element(by.name('type')).click();
    element(by.css(`.mat-option[ng-reflect-value="${value}"]`)).click();
  }

  getColorSelect(value) {
    element(by.name('color')).click();
    element(by.css(`.mat-option[ng-reflect-value="${value}"]`)).click();
  }

  getBrandSelect(value) {
    element(by.name('brand')).click();
    element(by.css(`.mat-option[ng-reflect-value="${value}"]`)).click();
  }

  getClearFilterButton() {
    return element(by.css('button.clear-filters'));
  }
}
