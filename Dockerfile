FROM node:8.6.0

# general setup
MAINTAINER nicholaschris
WORKDIR /srv
ENV TERM vt220
EXPOSE 80

# vars
ENV ng /srv/app/node_modules/@angular/cli/bin/ng
ENV APACHE_LOCK_DIR /var/lock/apache2/
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_LOG_DIR /var/log/apache2/
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data

# required packages
RUN apt-get update && apt-get install -y bash curl vim apache2


RUN mkdir -p /var/run/apache2/ /var/lock/apache2/ && chown www-data:www-data /var/run/apache2/ /var/lock/apache2/
ADD . /srv/app/
RUN cd /srv/app && rm -rf node_modules && npm install && npm run build
RUN mkdir -p /var/www/html/ && cp -pr /srv/app/dist/* /var/www/html/

ENTRYPOINT ["/usr/sbin/apache2", "-D", "FOREGROUND"]
